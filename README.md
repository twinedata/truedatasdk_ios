# Getting Started with TrueData SDK (Version 7.0.0)

## Requirements

1. Xcode 9.0 and above
2. iOS target of 9.3 and above
3. iOS device for testing (Location parameters will update on device only)

## Setup

We have created an SDK for your app to make it easy to include TrueData in your application. Please follow the steps below to integrate the SDK.

### Obtain a TrueData Token and Tag

If you have not yet received a token and tag, please request one from your TrueData contact.

### Update Info.plist

Add the following permissions:

> `Privacy - Bluetooth Peripheral Usage Description`
>
> `Privacy - Location When In Use Usage Description`

If you have a justifiable reason to be getting the user's location in the background, add this permission as well:

> `Privacy - Location Always and When In Use Usage Description`
>
> `Privacy - Location Always Usage Description`

For the _Required background modes_ key, add the following values:

> `App communicates using CoreBluetooth*`
>
> `App shares data using CoreBluetooth` 
>
> `App downloads content from the network`

If you are using background location, you will also need to add location to the _Required background modes_:

> `App registers for location updates`

_Note that Privacy - Bluetooth Peripheral Usage Description will not add a user facing permission to your app_

Make sure that your app is asking for the correct location permissions, either when in use or always from the user. You must have the appropriate method calls to prompt the user for location permissions in your app code (AppDelegate, MainViewController etc.) to enable location tracking:

> `locationManager.requestWhenInUseAuthorization()`
>
> `locationManager.requestAlwaysAuthorization()`

\*If the App Store asks you why Bluetooth is necessary, feel free to include the following comment: "Bluetooth data helps contextualize the user's location, complementing lat/long coordinates with rich, time-specific BLE associations integral to defining a user's precise location in relative terms."

## Installation: CocoaPods

Install Cocoapods in your project if it is not already installed, then add the following lines to your Podfile below your target:

Update your cocoapods repos with `pod repo update` and/or with `pod install --repo-update`

At the top of the podfile:

> `use_frameworks!`

In the body:

> `pod 'TrueDataSDK'`

## Usage: Objective C Apps

### 1. At the top of your AppDelegate.m file (or any other file where you plan to use the SDK), import the SDK:

> `#import <TrueDataSDK/TrueDataSDK.h>`

### 2. Inside didFinishLaunchingWithOptions method, register your app:

> `[TrueData registerAppWithUsername:"YOUR-USERNAME" password:"YOUR-PASSWORD" token:"YOUR-TOKEN" willUseCustomLocation:NO];`

### This will automatically begin scanning and sending the majority of data to be collected, however some data (such as phone, email or age) will require user input before it can be sent, and therefore, it must be submitted manually:

### 3. To pass in custom event information:

> `[[TrueData shared] sendEvent:"name" details:"details"];`

### 4. To pass in phone/email information:

> `[[TrueData shared] sendIdentity:"email" phoneNumber:"phone"];`

### 5. To pass in demographic information, either pass in all five parameters (passing an empty string for any paremeters you don't have):

> `[[TrueData shared] sendDemographics:"M/F" birthday:"yyyymmdd" age:"aa" ageRange:"aa-aa"];`

### 9. To pass in Consent String information(GDPR if applicable):

> `[[TrueData shared] cmp];`

## Usage: Swift Apps

### 1. At the top of your AppDelegate file (or any other file where you plan to use the SDK), import the SDK:

> `import TrueDataSDK`

### 2. In your AppDelegate file, inside the the didFinishLaunchingWithOptions method, add the the token, tag, username and password you have been provided:

> `TrueData.registerApp(withUsername: "YOUR-USERNAME", password: "YOUR-PASSWORD", token: "YOUR-TOKEN", willUseCustomLocation: false)`

### This will automatically begin scanning and sending the majority of data to be collected, however some data (such as phone, email or age) will require user input before it can be sent, and therefore, it must be submitted manually:

### 3. To pass in event information:

> `TrueData.shared().sendEvent("event name", details: "details")`

### 4. To pass in phone/email information\*\*:

> `TrueData.shared().sendIdentity("Email", phone: "Phone")`

### 5. To pass in demographic information, either pass in all five parameters (passing an empty string for any paremeters you don't have):

> `TrueData.shared().sendDemographics("M/F", birthDay: "yyyymmdd", age: "22", ageRange: "22-23")`

### 6. To pass in Consent String information(GDPR if applicable):

> `TrueData.shared().cmp()`

\*\* We use HTTPS for all of our server calls from the client and hash email and phone numbers using three techniques (MD5, SHA1, SHA256) before sending this data up.

### Verify that information is being sent

To verify that scans are occuring and information is being successfully sent, check the logs:

### Add TrueData.shared().enableLogging() after registration. Ensure that this is not left in your release builds as it will expose api tokens in the debug logs. You should see the following to confirm data is being successfully sent periodically:

```diff
[TrueDataSDK] <2020-02-24 09:21:25.035> {
    FailedRecordCount = 0;
    Records =     (
                {
            SequenceNumber = 49602622809189221625378807733246258009730311033548963842;
            ShardId = "shardId-000000000000";
        }
    );
} Kinesis response
```
